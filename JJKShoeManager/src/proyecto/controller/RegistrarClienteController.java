package proyecto.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import proyecto.bsn.ClienteBsn;
import proyecto.bsn.exception.ClienteYaExisteException;
import proyecto.dao.ClienteDAO;
import proyecto.dao.impl.ClienteDAONIO;
import proyecto.model.Cliente;

import java.io.IOException;
import java.util.List;

public class RegistrarClienteController {
    @FXML
    private TextField txtId;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtTelefono;


    @FXML
    private TableView<Cliente> tblClientes;
    @FXML
    private TableColumn<Cliente, String> clmId;
    @FXML
    private TableColumn<Cliente, String> clmNombre;
    @FXML
    private TableColumn<Cliente, String> clmTelefono;

    private ClienteBsn clienteBsn;

    private ClienteDAONIO clienteDAONIO;

    public RegistrarClienteController() {
        this.clienteBsn = new ClienteBsn();
    }


    @FXML
    public void initialize(){
        clmId.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
        clmNombre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombre()));
        clmTelefono.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTelefono()));
        refrescarTabla();
    }

    public void btnGuardar_action() {
        String idIngresado = txtId.getText().trim();
        String nombreIngresado = txtNombre.getText().trim();
        String telefonoIngresado = txtTelefono.getText().trim();
        nombreIngresado = nombreIngresado.toLowerCase();

        if (idIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de cliente", "Resultado de la transacción", "El id es requerido");
            txtId.requestFocus();
            return;
        }
        try {
            Integer.parseInt(idIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de cliente", "Resultado de la transacción", "El id debe ser un valor numérico");
            txtId.requestFocus();
            txtId.clear();
            return;
        }

        if (nombreIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de cliente", "Resultado de la transacción", "El nombre es requerido");
            txtNombre.requestFocus();
            return;
        }

        try {
            Long.parseLong(telefonoIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de cliente", "Resultado de la transacción", "El telefono debe ser un valor numerico");
            txtTelefono.requestFocus();
            txtTelefono.clear();
            return;
        }

        if (telefonoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de cliente", "Resultado de la transacción", "El telefono es requerido");
            txtTelefono.requestFocus();
            return;
        }


        Cliente cliente = new Cliente(idIngresado, nombreIngresado, telefonoIngresado);
        try {
            this.clienteBsn.registrarCliente(cliente);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de cliente", "Resultado de la transacción", "El cliente ha sido registrado con éxito");
            limpiarCampos();
            Cliente clienteClon = cliente.clone();
            tblClientes.getItems().add(clienteClon);
        } catch (ClienteYaExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de cliente", "Resultado de la transacción", e.getMessage());
        } catch (CloneNotSupportedException cnse){
            this.refrescarTabla();
        }
    }

    public void btnConsultar_action() throws IOException {

        String idIngresado = txtId.getText().trim();
        String nombreIngresado = txtNombre.getText().trim();
        String telefonoIngresado = txtTelefono.getText().trim();
        nombreIngresado = nombreIngresado.toLowerCase();

        if (idIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de cliente", "Resultado de la transacción", "El id es requerido");
            txtId.requestFocus();
            return;
        }
        try {
            Integer.parseInt(idIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de cliente", "Resultado de la transacción", "El id debe ser un valor numérico");
            txtId.requestFocus();
            txtId.clear();
            return;
        }



        Cliente cliente = new Cliente(idIngresado, nombreIngresado, telefonoIngresado);
        List<Cliente> clientesList = clienteBsn.consultarClientes();

        if(clientesList.contains(cliente)){
            mostrarMensaje(Alert.AlertType.INFORMATION, "Consulta de cliente", "Resultado de la transacción", "El cliente ya está registrado");
        } else {
            mostrarMensaje(Alert.AlertType.ERROR, "Consulta de cliente", "Resultado de la transacción", "El cliente no esta registrado");
        }

    }


    private boolean validarCampos(String... campos) {
        boolean sonValidos = true;
        for (String campo : campos) {
            if (campo == null || campo.trim().isEmpty()) {
                sonValidos = false;
                break;
            }
        }
        return sonValidos;
    }

    private void limpiarCampos() {
        this.txtId.clear();
        this.txtNombre.clear();
        this.txtTelefono.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    private void refrescarTabla(){
        List<Cliente> clientesList = clienteBsn.consultarClientes();
        ObservableList<Cliente> clientesListObservable = FXCollections.observableList(clientesList);
        tblClientes.setItems(clientesListObservable);
    }
}
