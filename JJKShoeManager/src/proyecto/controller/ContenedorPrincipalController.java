package proyecto.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javax.swing.*;
import java.io.IOException;

public class ContenedorPrincipalController {

    @FXML
    private BorderPane contenedorPrincipal;
    @FXML
    private PasswordField txtContra;
    @FXML
    private MenuBar barraMenu;
    @FXML
    private HBox cajitaFachera;

    public void btnRegistrarProducto_action() throws IOException {
        Parent ventanaRegistroProducto = FXMLLoader.load(getClass().getResource("../view/registrar-producto.fxml"));
        this.contenedorPrincipal.setCenter(ventanaRegistroProducto);
    }

    public void btnRegistrarCliente_action() throws IOException {
        Parent ventanaRegistroCliente = FXMLLoader.load(getClass().getResource("../view/registrar-cliente.fxml"));
        this.contenedorPrincipal.setCenter(ventanaRegistroCliente);
    }

    public void btnRegistrarVenta_action() throws IOException {
        Parent ventanaRegistroVenta = FXMLLoader.load(getClass().getResource("../view/registrar-venta.fxml"));
        this.contenedorPrincipal.setCenter(ventanaRegistroVenta);
    }


    public void btnSalir_action() throws IOException{
        System.exit(0);
    }

    public void btnContinuar_action(){
        if(txtContra.getText().equals("por5favor")){
            barraMenu.setVisible(true);
            cajitaFachera.setVisible(false);
        }else{
            txtContra.clear();
            mostrarMensaje(Alert.AlertType.ERROR, "Ingreso de Usuario", "Ingreso inválido", "Contraseña incorrecta");
            txtContra.requestFocus();
        }
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
