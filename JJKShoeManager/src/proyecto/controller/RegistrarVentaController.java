package proyecto.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;


import proyecto.bsn.exception.VentaYaExisteException;
import proyecto.dao.impl.VentaDAONIO;

import proyecto.model.Venta;
import proyecto.bsn.VentaBsn;
import java.util.List;

public class RegistrarVentaController {

    @FXML
    private TextField txtId;
    @FXML
    private TextField txtFecha;
    @FXML
    private TextField txtValor;
    @FXML
    private TextField txtCliente;
    @FXML
    private TableView<Venta> tblVentas;
    @FXML
    private TableColumn<Venta, String> clmId;
    @FXML
    private TableColumn<Venta, String> clmFecha;
    @FXML
    private TableColumn<Venta, String> clmValor;
    @FXML
    private TableColumn<Venta, String> clmCliente;



    private VentaBsn ventaBsn;
    private VentaDAONIO ventaDAONIO;

    public RegistrarVentaController() {
        ventaBsn = new VentaBsn();
    }

    public VentaBsn getVentaBsn() {
        return ventaBsn;
    }

    @FXML
    public void initialize(){
        clmId.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFecha()));
        clmValor.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getValor()));
        clmCliente.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCliente()));
        refrescarTabla();
    }


    public void btnGuardar_action() {
        String idIngresado = txtId.getText().trim();
        String fechaIngresada = txtFecha.getText().trim();
        String valorIngresado = txtValor.getText().trim();
        String clienteIngresado = txtCliente.getText().trim();

        try {
            Integer.parseInt(idIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de venta", "Resultado de la transacción", "El id debe ser un valor numérico");
            txtId.requestFocus();
            txtId.clear();
            return;
        }

        if (idIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de venta", "Resultado de la transacción", "El id es requerido");
            txtId.requestFocus();
            return;
        }


        if (fechaIngresada.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de venta", "Resultado de la transacción", "Los nombres son requeridos");
            txtFecha.requestFocus();
            return;
        }

        try {
            Integer.parseInt(valorIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El valor debe ser un valor numérico");
            txtValor.requestFocus();
            txtValor.clear();
            return;
        }

        if (valorIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El valor es requerido");
            txtValor.requestFocus();
            return;
        }

        try {
            Integer.parseInt(clienteIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El Id del cliente debe ser un valor numérico");
            txtValor.requestFocus();
            txtValor.clear();
            return;
        }

        if (clienteIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de producto", "Resultado de la transacción", "El id del cliente es requerido");
            txtCliente.requestFocus();
            return;
        }




        Venta venta = new Venta(idIngresado, fechaIngresada, valorIngresado, clienteIngresado);
        List<Venta> ventasList = ventaBsn.consultarVentas();

        refrescarTabla();
        if(ventasList.contains(venta)){
            mostrarMensaje(Alert.AlertType.ERROR, "Consulta de producto", "Resultado de la transacción", "El producto ya está registrado");
            return;
        }

        try {
            this.ventaBsn.registrarVenta(venta);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de venta", "Resultado de la transacción", "La venta ha sido registrada con éxito");
            limpiarCampos();
            Venta ventaClon = venta.clone();
            tblVentas.getItems().add(ventaClon);
        } catch (VentaYaExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de venta", "Resultado de la transacción", e.getMessage());
        } catch (CloneNotSupportedException cnse){
            this.refrescarTabla();
        }
        refrescarTabla();
    }



    private boolean validarCampos(String... campos) {
        boolean sonValidos = true;
        for (String campo : campos) {
            if (campo == null || campo.trim().isEmpty()) {
                sonValidos = false;
                break;
            }
        }
        return sonValidos;
    }


    private void limpiarCampos() {
        this.txtId.clear();
        this.txtFecha.clear();
        this.txtValor.clear();
        this.txtCliente.clear();
    }


    public void refrescarTabla(){
        List<Venta> ventasList = ventaBsn.consultarVentas();
        ObservableList<Venta> ventasListObservable = FXCollections.observableList(ventasList);
        tblVentas.setItems(ventasListObservable);
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
