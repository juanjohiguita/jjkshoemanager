package proyecto.dao.impl;

import proyecto.dao.ProductoDAO;
import proyecto.model.Producto;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class ProductoDAONIO implements ProductoDAO {

    private final static String NOMBRE_ARCHIVO = "productos";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private static final String FIELD_SEPARATOR = ",";
    private static final String RECORD_SEPARATOR = System.lineSeparator();

    public ProductoDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarProducto(Producto producto) {
        String registro = parseProductoString(producto);
        byte[] datosRegistro = registro.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseProductoString(Producto producto) {
        StringBuilder sb = new StringBuilder();
        sb.append(producto.getId()).append(FIELD_SEPARATOR)
                .append(producto.getTalla()).append(FIELD_SEPARATOR)
                .append(producto.getMarca()).append(FIELD_SEPARATOR)
                .append(producto.getValor()).append(FIELD_SEPARATOR)
                .append(producto.getGenero()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public Optional<Producto> consultarPorId(String id) {

        return Optional.empty();
    }

    @Override
    public List<Producto> consultarProductos() {
        List<Producto> productos = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(productoString -> {
                Producto productoConvertido = parseProductoObject(productoString);
                productos.add(productoConvertido);
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return productos;
    }

    private Producto parseProductoObject(String productoString) {
        String[] datosProducto = productoString.split(FIELD_SEPARATOR);
        Producto producto = new Producto(datosProducto[0], datosProducto[1],
                datosProducto[2], datosProducto[3], datosProducto[4]);
        return producto;
    }

    public static boolean removeLine(String Line) throws IOException{
        boolean retorno = false;
        BufferedReader br = new BufferedReader(new FileReader(ARCHIVO.toString()));
        File temp = new File("temp.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
        String removeID = Line;
        String currentLine;
        while((currentLine = br.readLine()) != null){
            String trimmedLine = currentLine.trim();
            if(trimmedLine.equals(removeID)){
                System.out.println("entra :)");
                currentLine = "";

            }
            if(!currentLine.isEmpty()){
                bw.write(currentLine + System.getProperty("line.separator"));
                System.out.println("entra pero fuera xd");
                retorno = true;
            }

        }
        bw.close();
        br.close();
        boolean delete = ARCHIVO.toFile().delete();
        boolean b = temp.renameTo(ARCHIVO.toFile());

        System.out.println(delete);
        System.out.println(b);
        return retorno;
    }
}
