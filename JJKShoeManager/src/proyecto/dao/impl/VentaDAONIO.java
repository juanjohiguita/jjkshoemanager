package proyecto.dao.impl;

import proyecto.dao.VentaDAO;
import proyecto.model.Producto;
import proyecto.model.Venta;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class VentaDAONIO implements VentaDAO{
    private final static String NOMBRE_ARCHIVO = "ventas";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private static final String FIELD_SEPARATOR = ",";
    private static final String RECORD_SEPARATOR = System.lineSeparator();

    public VentaDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarVenta(Venta venta) {
        String registro = parseVentaString(venta);
        byte[] datosRegistro = registro.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseVentaString(Venta venta) {
        StringBuilder sb = new StringBuilder();
        sb.append(venta.getId()).append(FIELD_SEPARATOR)
                .append(venta.getFecha()).append(FIELD_SEPARATOR)
                .append(venta.getValor()).append(FIELD_SEPARATOR)
                .append(venta.getCliente()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public Optional<Venta> consultarPorId(String id) {

        return Optional.empty();
    }

    @Override
    public List<Venta> consultarVentas() {
        List<Venta> ventas = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(ventaString -> {
                Venta ventaConvertido = parseVentaObject(ventaString);
                ventas.add(ventaConvertido);
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return ventas;
    }

    private Venta parseVentaObject(String ventaString) {
        String[] datosVenta = ventaString.split(FIELD_SEPARATOR);
        Venta venta = new Venta(datosVenta[0], datosVenta[1],
                datosVenta[2], datosVenta[3]);
        return venta;
    }

    public static boolean removeLine(String Line) throws IOException{
        boolean retorno = false;
        BufferedReader br = new BufferedReader(new FileReader(ARCHIVO.toString()));
        File temp = new File("temp.txt");
        BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
        String removeID = Line;
        String currentLine;
        while((currentLine = br.readLine()) != null){
            String trimmedLine = currentLine.trim();
            if(trimmedLine.equals(removeID)){
                System.out.println("entra :)");
                currentLine = "";

            }
            if(!currentLine.isEmpty()){
                bw.write(currentLine + System.getProperty("line.separator"));
                System.out.println("entra pero fuera xd");
                retorno = true;
            }

        }
        bw.close();
        br.close();
        boolean delete = ARCHIVO.toFile().delete();
        boolean b = temp.renameTo(ARCHIVO.toFile());

        System.out.println(delete);
        System.out.println(b);
        return retorno;
    }

}
