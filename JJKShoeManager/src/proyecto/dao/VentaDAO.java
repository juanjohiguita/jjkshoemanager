package proyecto.dao;

import proyecto.model.Venta;
import java.util.List;
import java.util.Optional;

public interface VentaDAO {

    void registrarVenta (Venta venta);

    Optional<Venta> consultarPorId(String id);

    List<Venta> consultarVentas();
}
