package proyecto.model;

import javafx.scene.control.Alert;

public class Cliente {
    String nombre;
    String id;
    String telefono;

    public Cliente(String id, String nombre, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    @Override
    public Cliente clone() throws CloneNotSupportedException {
        return (Cliente) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if(o == this){
            return true;
        }
        if(!(o instanceof Cliente)) {
            return false;
        }
        Cliente cl = (Cliente)o;
        if(cl.getId().equals(this.getId()) && !(cl.getNombre().equals(this.getNombre()) && !cl.getTelefono().equals(this.getTelefono()))){
            return true;
        }
        if(cl.getId().equals(this.getId()) && cl.getNombre().equals(this.getNombre()) && cl.getTelefono().equals(this.getTelefono())){
            return true;
        } else {
            return false;
        }
    }
    @Override
    public String toString(){
        return this.id.toString() + "," + this.nombre.toString() + "," + this.telefono.toString();
    }

}
