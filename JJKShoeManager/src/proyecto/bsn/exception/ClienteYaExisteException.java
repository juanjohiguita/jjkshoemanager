package proyecto.bsn.exception;

public class ClienteYaExisteException extends Exception {

    public ClienteYaExisteException() { super("Ya existe un cliente con el id ingresado");
    }
}
