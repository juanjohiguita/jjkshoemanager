package proyecto.bsn;

import proyecto.bsn.exception.ProductoYaExisteException;
import proyecto.dao.ProductoDAO;
import proyecto.dao.impl.ProductoDAONIO;
import proyecto.model.Producto;

import java.util.List;
import java.util.Optional;

public class ProductoBsn {

    private ProductoDAO productoDAO;

    public ProductoBsn() { this.productoDAO = new ProductoDAONIO(); }


    public void registrarProducto(Producto producto) throws ProductoYaExisteException {
        Optional<Producto> productoOptional = this.productoDAO.consultarPorId(producto.getId());
        if (productoOptional.isPresent()) {
            throw new ProductoYaExisteException();
        } else {
            this.productoDAO.registrarProducto(producto);
        }
    }
    public boolean consultarProducto(Producto producto)  {
        Optional<Producto> productoOptional = this.productoDAO.consultarPorId(producto.getId());
        if (productoOptional.isPresent()) {
            return true;
        } else {
            return false;
        }
    }

    public List<Producto> consultarProductos() { return productoDAO.consultarProductos(); }
}
